#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "proc-common.h"
#include "tree.h"

#define SLEEP_PROC_SEC  10
#define SLEEP_TREE_SEC  3

void fork_procs(struct tree_node *root)
{
	printf("PID = %ld, name %s, starting...\n",
               (long)getpid(), root->name);
	change_pname(root->name);

        if (root->nr_children == 0)
          sleep(SLEEP_PROC_SEC);

        int i;
        pid_t p;
        for (i = 0; i < root->nr_children; ++i) {
          p = fork();
          if (p < 0) {
            perror("fork");
            exit(1);
          }
          if (p == 0) {
            fork_procs(&root->children[i]);
          }
        }

        int status;
        for (i = 0; i < root->nr_children; ++i) {
          p = wait(&status);
          explain_wait_status(p, status);
        }

        printf("PID = %ld, name = %s, exiting...\n",
               (long)getpid(), root->name);
	exit(0);
}

/*
 * The initial process forks the root of the process tree,
 * waits for the process tree to be completely created,
 * then takes a photo of it using show_pstree().
 *
 * How to wait for the process tree to be ready?
 * In ask2-{fork, tree}:
 *      wait for a few seconds, hope for the best.
 * In ask2-signals:
 *      use wait_for_ready_children() to wait until
 *      the first process raises SIGSTOP.
 */
int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <input_tree_file>\n\n", argv[0]);
		exit(1);
	}

        struct tree_node *root;
	root = get_tree_from_file(argv[1]);

	pid_t pid;
	int status;

	/* Fork root of process tree */
	pid = fork();
	if (pid < 0) {
		perror("main: fork");
		exit(1);
	}
	if (pid == 0) {
		/* Child */
		fork_procs(root);
		exit(1);
	}

	/*
	 * Father
	 */
	/* for ask2-signals */
	/* wait_for_ready_children(1); */

	/* for ask2-{fork, tree} */
	sleep(SLEEP_TREE_SEC);

	/* Print the process tree root at pid */
	show_pstree(pid);

	/* for ask2-signals */
	/* kill(pid, SIGCONT); */

	/* Wait for the root of the process tree to terminate */
	pid = wait(&status);
	explain_wait_status(pid, status);

	return 0;
}
