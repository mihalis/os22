#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

void doWrite(int fd, const char *buff, int len) {
  int w, i = 0;

  while (i < len) {
    w = write(fd, buff + i, len - i);
    if (w < 0)
      perror("Write"),
      exit(EXIT_FAILURE);
    i += w;
  }
}

void write_file(int fd, const char *infile) {
  char buff[1024];
  int row_idx;
  int fd_src = open(infile, O_RDONLY);

  if (fd_src < 0)
    perror(infile),
    exit(EXIT_FAILURE);

  while (true) {
    row_idx = read(fd_src, buff, sizeof(buff) - 1);

    if (row_idx < 0)
      perror("Read"),
      exit(EXIT_FAILURE);

    if (!row_idx)
      return ;

    buff[row_idx] = '\0';
    doWrite(fd, buff, strlen(buff));
  }
}

int main(int argc, char *argv[]) {
  if (argc < 3)
    printf("Usage: ./fcon file+ outfile\n"),
    exit(EXIT_SUCCESS);

  int i, file_count = argc - 1;
  char *outfile = argv[file_count];

  for (i = 1; i < file_count; ++i)
    if (!strcmp(argv[i], argv[file_count]))
      fprintf(stderr, "Error: infile%d is the same as outfile.\n", i),
      exit(EXIT_FAILURE);

  int fd = open( outfile
               , O_CREAT  // Create file if it doesn't exist.
               | O_WRONLY // Only write on the file.
               | O_TRUNC  // Write over it's contents.
               , S_IWUSR  // User can write.
               | S_IRUSR  // User can read.
               );
  if (fd < 0)
    perror(outfile),
    exit(EXIT_FAILURE);

  for (i = 1; i < file_count; ++i)
    write_file(fd, argv[i]);

  if (close(fd) < 0)
    perror(outfile),
    exit(EXIT_FAILURE);

  return 0;
}
