/*
 * th-mandel.c
 *
 * A program to draw the Mandelbrot Set on a 256-color xterm.
 *
 */

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <signal.h>

#include <sys/mman.h>
#include <sys/wait.h>
#include <semaphore.h>

#include "mandel-lib.h"

sem_t * mutexes;

void * turn;

int NPROCS = 3;

/*
 * POSIX thread functions do not return error numbers in errno,
 * but in the actual return value of the function call instead.
 * This macro helps with error reporting in this case.
 */
#define perror_pthread(ret, msg) \
	do { errno = ret; perror(msg); } while (0)

#define MANDEL_MAX_ITERATION 100000

/***************************
 * Compile-time parameters *
 ***************************/

/*
 * Output at the terminal is is x_chars wide by y_chars long
*/
int y_chars = 50;
int x_chars = 90;

int mandel_set[50][90];

/*
 * The part of the complex plane to be drawn:
 * upper left corner is (xmin, ymax), lower right corner is (xmax, ymin)
*/
double xmin = -1.8, xmax = 1.0;
double ymin = -1.0, ymax = 1.0;

/*
 * Every character in the final output is
 * xstep x ystep units wide on the complex plane.
 */
double xstep;
double ystep;

/*
 * This function computes a line of output
 * as an array of x_char color values.
 */
void compute_mandel_line(int line, int color_val[])
{
	/*
	 * x and y traverse the complex plane.
	 */
	double x, y;

	int n;
	int val;

	/* Find out the y value corresponding to this line */
	y = ymax - ystep * line;

	/* and iterate for all points on this line */
	for (x = xmin, n = 0; n < x_chars; x+= xstep, n++) {

		/* Compute the point's color value */
		val = mandel_iterations_at_point(x, y, MANDEL_MAX_ITERATION);
		if (val > 255)
			val = 255;

		/* And store it in the color_val[] array */
		val = xterm_color(val);
		color_val[n] = val;
	}
}

/*
 * This function outputs an array of x_char color values
 * to a 256-color xterm.
 */
void output_mandel_line(int fd, int color_val[])
{
	int i;

	char point ='@';
	char newline='\n';

	for (i = 0; i < x_chars; i++) {
		/* Set the current color, then output the point */
		set_xterm_color(fd, color_val[i]);
		if (write(fd, &point, 1) != 1) {
			perror("compute_and_output_mandel_line: write point");
			exit(1);
		}
	}

	/* Now that the line is done, output a newline character */
	if (write(fd, &newline, 1) != 1) {
		perror("compute_and_output_mandel_line: write newline");
		exit(1);
	}
}

void compute_and_output_mandel_line(int fd, int line)
{
	/*
	 * A temporary array, used to hold color values for the line being drawn
	 */
	int color_val[x_chars];

	compute_mandel_line(line, color_val);
	output_mandel_line(fd, color_val);
}

void thread_fun(int n)
{
  int i, local_turn;
  for (i = n; i < y_chars; i += NPROCS)
    compute_mandel_line(i, mandel_set[i]);

  local_turn = n;
  sscanf(turn, "%i", &i);
  while (i < y_chars) {
    if (i == local_turn) {
      output_mandel_line(1, mandel_set[i]);
      local_turn += NPROCS;
      ++i;
      sprintf((char *) turn, "%i", i);
    }
    sscanf(turn, "%i", &i);
  }
}

void interupt_handler() {
  reset_xterm_color(1);
  printf("Program Interupted\n");
  exit(1);
}

int main(int argc, char *argv[])
{
  pid_t p;

  if (argc == 2)
    NPROCS = atoi(argv[1]);

  xstep = (xmax - xmin) / x_chars;
  ystep = (ymax - ymin) / y_chars;

  int i;
  turn = mmap(NULL, 0x10000,
		PROT_READ | PROT_WRITE,
		MAP_SHARED | MAP_ANON,
		-1, 0);

  memset(turn, '0', 0x10000);
  for (i = 0; i < NPROCS; ++i) {
    p = fork();
    if (p < 0) {
        perror("fork");
        exit(-1);
    }
    if (p == 0) {
        thread_fun(i);
        return 0;
    }
  }
  for (i = 0; i < NPROCS; ++i)
    wait(NULL);

  // C-c
  signal(SIGINT, interupt_handler);

  munmap(turn, 0x10000);

  reset_xterm_color(1);
  return 0;
}
